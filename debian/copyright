Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: laniakea-spark
Upstream-Contact: Matthias Klumpp <mak@debian.org>
Source: https://github.com/lkhq/laniakea-spark

Files: *
Copyright: 2016-2024, Matthias Klumpp <mak@debian.org>
License: LGPL-3.0-or-later


Files: debian/*
Copyright: 2014, Matthias Klumpp <mak@debian.org>
License: LGPL-3.0-or-later

Files: spark/runners/debspawn.py
       spark/utils/deb822.py
Copyright: 2014, Jon Severinsson <jon@severinsson.net>
           2016-2018, Matthias Klumpp <mak@debian.org>
           2012-2013, Paul Tagliamonte <paultag@debian.org>
License: Expat

License: LGPL-3.0-or-later
 This library is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation; either version 3 of the
 License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library. If not, see
 <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License version 3 can be found in
 `/usr/share/common-licenses/LGPL-3’.

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and/or associated documentation files (the "Materials"), to
 deal in the Materials without restriction, including without limitation the
 rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 sell copies of the Materials, and to permit persons to whom the Materials are
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice(s) and this permission notice shall be included in
 all copies or substantial portions of the Materials.
 .
 THE MATERIALS ARE PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 .
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE MATERIALS OR THE
 USE OR OTHER DEALINGS IN THE MATERIALS.
